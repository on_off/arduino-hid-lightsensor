#include <Arduino.h>
#include <Wire.h>
#include <BH1750.h>
#include <BetterSensor.h>
#include <MsTimer2.h>

BH1750 lightMeter;

void send() {
  BetterSensor.send();
}

void setup()
{
  Wire.begin();
  lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE);
  BetterSensor.enableFeatureReport();

  MsTimer2::set(250, send);
  MsTimer2::start();
}

void loop()
{
  float lux = lightMeter.readLightLevel();
  BetterSensor.updateReport((HID_ULONG)(lux * 100));
}