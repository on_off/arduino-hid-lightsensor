#include "Sensor.h"

#if defined(_USING_HID)

//================================================================================
//================================================================================
//	Sensor

static const uint8_t _hidReportDescriptor[] PROGMEM = {
    // HID_USAGE_PAGE_SENSOR,
    // HID_USAGE_SENSOR_TYPE_COLLECTION,
    // HID_COLLECTION(Application),

    //     HID_REPORT_ID(1),
        HID_USAGE_PAGE_SENSOR, // USAGE_PAGE (Sensor)

        HID_USAGE_SENSOR_TYPE_LIGHT_AMBIENTLIGHT, // USAGE (AmbientLight)
        HID_COLLECTION(Physical),
        // HID_COLLECTION(Application),
            
            HID_REPORT_ID(1), //TEST
//ass
// feature reports (xmit/receive)
            HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_PROPERTY_SENSOR_CONNECTION_TYPE, // NAry
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(2),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_INTEGRATED_SEL,
                HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_ATTACHED_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(5),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL_WAKE_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL_WAKE_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_WAKE_SEL_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_PROPERTY_POWER_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(5),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE_UNDEFINED_SEL,
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D1_LOW_POWER_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D2_STANDBY_WITH_WAKE_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D3_SLEEP_WITH_WAKE_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D4_POWER_OFF_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(6),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_STATE_UNKNOWN_SEL_SEL,
                HID_USAGE_SENSOR_STATE_READY_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NO_DATA_SEL_SEL,
                HID_USAGE_SENSOR_STATE_INITIALIZING_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ERROR_SEL_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_PROPERTY_REPORT_INTERVAL,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            HID_REPORT_SIZE(32),
            HID_REPORT_COUNT(1),
            HID_UNIT_EXPONENT(0),
            HID_FEATURE(Data_Var_Abs),
//ass

            // HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE, HID_USAGE_SENSOR_DATA_MOD_CHANGE_SENSITIVITY_REL_PCT),
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_16(0x10, 0x27), // 10000 = 0.00 to 100.00 percent with 2 digits past decimal point
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(1),
            // HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digits past decimal point
            // HID_FEATURE(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE, HID_USAGE_SENSOR_DATA_MOD_MAX),
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            // HID_REPORT_SIZE(32),
            // HID_REPORT_COUNT(1),
            // HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digit past decimal point
            // HID_FEATURE(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE, HID_USAGE_SENSOR_DATA_MOD_MIN),
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            // HID_REPORT_SIZE(32),
            // HID_REPORT_COUNT(1),
            // HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digit past decimal point
            // HID_FEATURE(Data_Var_Abs),

            // add this definition if required by the specific application
            // HID_USAGE_SENSOR_PROPERTY_RESPONSE_CURVE,
            // HID_LOGICAL_MIN_16(0x01, 0x80), //    LOGICAL_MINIMUM (-32767)
            // HID_LOGICAL_MAX_16(0xFF, 0x7F), //    LOGICAL_MAXIMUM (32767)
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(10),   //as required for n pair of values
            // HID_UNIT_EXPONENT(0x0), // scale default unit to provide 0 digits past the decimal point
            // HID_FEATURE(Data_Var_Abs),

            //input reports (transmit)
            HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(6),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),

            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_STATE_UNKNOWN_SEL_SEL,
                HID_USAGE_SENSOR_STATE_READY_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NO_DATA_SEL_SEL,
                HID_USAGE_SENSOR_STATE_INITIALIZING_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ERROR_SEL_SEL,
                HID_INPUT(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_EVENT,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(5),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),

            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_EVENT_UNKNOWN_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_PROPERTY_CHANGED_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_POLL_RESPONSE_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_CHANGE_SENSITIVITY_SEL_SEL,
                HID_INPUT(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digits past decimal point
            HID_REPORT_SIZE(32),
            HID_REPORT_COUNT(1),
            HID_INPUT(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA_LIGHT_COLOR_TEMPERATURE,
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_16(0xFF, 0xFF),
            // HID_UNIT_EXPONENT(0),
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(1),
            // HID_INPUT(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA_LIGHT_CHROMATICITY_X,
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_16(0xFF, 0xFF),
            // HID_UNIT_EXPONENT(0x0C), // scale default unit to provide 4 digits past decimal point
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(1),
            // HID_INPUT(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA_LIGHT_CHROMATICITY_Y,
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_16(0xFF, 0xFF),
            // HID_UNIT_EXPONENT(0x0C), // scale default unit to provide 4 digits past decimal point
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(1),
            // HID_INPUT(Data_Var_Abs),

        HID_END_COLLECTION,
    // HID_END_COLLECTION
};


Sensor_::Sensor_(void) 
{
	static HIDSubDescriptor node(_hidReportDescriptor, sizeof(_hidReportDescriptor));
	HID().AppendDescriptor(&node);
}

void Sensor_::begin(void)
{
    // _inputReport = {
    //     // HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL_ENUM,
    //     // HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL_ENUM,
    //     // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_ENUM,
    //     // HID_USAGE_SENSOR_STATE_READY_SEL_ENUM,
    //     // 500UL,
    //     // 200000UL,
    //     // 0UL,
    //     HID_USAGE_SENSOR_STATE_READY_SEL_ENUM,
    //     HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL_ENUM,
    //     0UL,
    // };
    _inputReport.ucSensorState = HID_USAGE_SENSOR_STATE_READY_SEL_ENUM;
    _inputReport.ucEventType = HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL_ENUM;
    _inputReport.usIlluminanceValue = 0;
}

void Sensor_::end(void)
{
}

int Sensor_::sendReport(ALS_INPUT_REPORT* inpRep)
{
	return HID().SendReport(2, inpRep, sizeof(ALS_INPUT_REPORT));
}

extern
const uint8_t _asciimap[128] PROGMEM;

uint8_t USBPutChar(uint8_t c);

// press() adds the specified key (printing, non-printing, or modifier)
// to the persistent key report and sends the report.  Because of the way 
// USB HID works, the host acts like the key remains pressed until we 
// call release(), releaseAll(), or otherwise clear the report and resend.
// size_t Keyboard_::press(uint8_t k) 
// {
// 	uint8_t i;
// 	if (k >= 136) {			// it's a non-printing key (not a modifier)
// 		k = k - 136;
// 	} else if (k >= 128) {	// it's a modifier key
// 		_keyReport.modifiers |= (1<<(k-128));
// 		k = 0;
// 	} else {				// it's a printing key
// 		k = pgm_read_byte(_asciimap + k);
// 		if (!k) {
// 			setWriteError();
// 			return 0;
// 		}
// 		if (k & 0x80) {						// it's a capital letter or other character reached with shift
// 			_keyReport.modifiers |= 0x02;	// the left shift modifier
// 			k &= 0x7F;
// 		}
// 	}
	
// 	// Add k to the key report only if it's not already present
// 	// and if there is an empty slot.
// 	if (_keyReport.keys[0] != k && _keyReport.keys[1] != k && 
// 		_keyReport.keys[2] != k && _keyReport.keys[3] != k &&
// 		_keyReport.keys[4] != k && _keyReport.keys[5] != k) {
		
// 		for (i=0; i<6; i++) {
// 			if (_keyReport.keys[i] == 0x00) {
// 				_keyReport.keys[i] = k;
// 				break;
// 			}
// 		}
// 		if (i == 6) {
// 			setWriteError();
// 			return 0;
// 		}	
// 	}
// 	sendReport(&_keyReport);
// 	return 1;
// }

// release() takes the specified key out of the persistent key report and
// sends the report.  This tells the OS the key is no longer pressed and that
// it shouldn't be repeated any more.
// size_t Keyboard_::release(uint8_t k) 
// {
// 	uint8_t i;
// 	if (k >= 136) {			// it's a non-printing key (not a modifier)
// 		k = k - 136;
// 	} else if (k >= 128) {	// it's a modifier key
// 		_keyReport.modifiers &= ~(1<<(k-128));
// 		k = 0;
// 	} else {				// it's a printing key
// 		k = pgm_read_byte(_asciimap + k);
// 		if (!k) {
// 			return 0;
// 		}
// 		if (k & 0x80) {							// it's a capital letter or other character reached with shift
// 			_keyReport.modifiers &= ~(0x02);	// the left shift modifier
// 			k &= 0x7F;
// 		}
// 	}
	
// 	// Test the key report to see if k is present.  Clear it if it exists.
// 	// Check all positions in case the key is present more than once (which it shouldn't be)
// 	for (i=0; i<6; i++) {
// 		if (0 != k && _keyReport.keys[i] == k) {
// 			_keyReport.keys[i] = 0x00;
// 		}
// 	}

// 	sendReport(&_keyReport);
// 	return 1;
// }

// void Keyboard_::releaseAll(void)
// {
// 	_keyReport.keys[0] = 0;
// 	_keyReport.keys[1] = 0;	
// 	_keyReport.keys[2] = 0;
// 	_keyReport.keys[3] = 0;	
// 	_keyReport.keys[4] = 0;
// 	_keyReport.keys[5] = 0;	
// 	_keyReport.modifiers = 0;
// 	sendReport(&_keyReport);
// }

size_t Sensor_::report(HID_ULONG lux)
{
    _inputReport.ucEventType = HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL_ENUM;
    _inputReport.usIlluminanceValue = lux;
	return sendReport(&_inputReport);
}

// size_t Keyboard_::write(uint8_t c)
// {
// 	uint8_t p = press(c);  // Keydown
// 	release(c);            // Keyup
// 	return p;              // just return the result of press() since release() almost always returns 1
// }

// size_t Keyboard_::write(const uint8_t *buffer, size_t size) {
// 	size_t n = 0;
// 	while (size--) {
// 		if (*buffer != '\r') {
// 			if (write(*buffer)) {
// 			  n++;
// 			} else {
// 			  break;
// 			}
// 		}
// 		buffer++;
// 	}
// 	return n;
// }

Sensor_ Sensor;

#endif

