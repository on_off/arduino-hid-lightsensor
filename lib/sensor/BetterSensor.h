/*
Copyright (c) 2014-2015 NicoHood
See the readme for credit to other people.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Include guard
#pragma once

#include <Arduino.h>
#include "HID.h"
#include "HID-Settings.h"
#include "hello.h"

//  Low level key report: up to 6 keys and shift, ctrl etc at once
typedef struct
{
    //common properties
    // HID_UCHAR ucReportId;
    HID_UCHAR ucConnectionType;
    HID_UCHAR ucReportingState;
    HID_UCHAR ucPowerState;
    HID_UCHAR ucSensorState;
    HID_ULONG ulReportInterval;

    //properties specific to this sensor
    HID_USHORT usIlluminanceChangeSensitivity;
    HID_ULONG usIlluminanceMaximum;
    HID_ULONG usIlluminanceMinimum;

    //add this definition if required by the specific application
    // HID_USHORT usResponseCurve[5][2]; //10 elements matches descriptor

} ALS_FEATURE_REPORT;

typedef struct
{
    //common values
    // HID_UCHAR ucReportId;
    HID_UCHAR ucSensorState;
    HID_UCHAR ucEventType;

    //values specific to this sensor
    HID_ULONG usIlluminanceValue;

} ALS_INPUT_REPORT;


class BetterSensor_ : public PluggableUSBModule
{
public:
    BetterSensor_(void);
    // uint8_t getLeds(void);
    // uint8_t getProtocol(void);
    // void wakeupHost(void);
    
    void setFeatureReport(void* report, int length){
        if(length > 0){
            featureReport = (uint8_t*)report;
            featureLength = length;
            
            // Disable feature report by default
            disableFeatureReport();
        }
    }
    
    int availableFeatureReport(void){
        if(featureLength < 0){
            return featureLength & ~0x8000;
        }
        return 0;
    }
    
    void enableFeatureReport(void){
        featureLength &= ~0x8000;
    }
    
    void disableFeatureReport(void){
        featureLength |= 0x8000;
    }

    void updateReport(HID_ULONG val);
    void setFeatureState(ALS_FEATURE_REPORT rep);
    
    virtual int send(void) final;

protected:
    // Implementation of the PUSBListNode
    int getInterface(uint8_t* interfaceCount) override;
    int getDescriptor(USBSetup& setup) override;
    bool setup(USBSetup& setup) override;
    
    EPTYPE_DESCRIPTOR_SIZE epType[1];
    uint8_t protocol;
    uint8_t idle;

    ALS_FEATURE_REPORT featureState;
    ALS_INPUT_REPORT _inputReport;
    
    // uint8_t leds;
    
    uint8_t* featureReport;
    int featureLength;
};
extern BetterSensor_ BetterSensor;


