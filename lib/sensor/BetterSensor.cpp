/*
Copyright (c) 2014-2015 NicoHood
See the readme for credit to other people.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "BetterSensor.h"

// static const uint8_t _hidReportDescriptorKeyboard[] PROGMEM = {
//     //  Keyboard
//     0x05, 0x01,                      /* USAGE_PAGE (Generic Desktop)	  47 */
//     0x09, 0x06,                      /* USAGE (Keyboard) */
//     0xa1, 0x01,                      /* COLLECTION (Application) */
//     0x05, 0x07,                      /*   USAGE_PAGE (Keyboard) */

//     /* Keyboard Modifiers (shift, alt, ...) */
//     0x19, 0xe0,                      /*   USAGE_MINIMUM (Keyboard LeftControl) */
//     0x29, 0xe7,                      /*   USAGE_MAXIMUM (Keyboard Right GUI) */
//     0x15, 0x00,                      /*   LOGICAL_MINIMUM (0) */
//     0x25, 0x01,                      /*   LOGICAL_MAXIMUM (1) */
//     0x75, 0x01,                      /*   REPORT_SIZE (1) */
// 	0x95, 0x08,                      /*   REPORT_COUNT (8) */
//     0x81, 0x02,                      /*   INPUT (Data,Var,Abs) */

//     /* Reserved byte, used for consumer reports, only works with linux */
// 	0x05, 0x0C,             		 /*   Usage Page (Consumer) */
//     0x95, 0x01,                      /*   REPORT_COUNT (1) */
//     0x75, 0x08,                      /*   REPORT_SIZE (8) */
//     0x15, 0x00,                      /*   LOGICAL_MINIMUM (0) */
//     0x26, 0xFF, 0x00,                /*   LOGICAL_MAXIMUM (255) */
//     0x19, 0x00,                      /*   USAGE_MINIMUM (0) */
//     0x29, 0xFF,                      /*   USAGE_MAXIMUM (255) */
//     0x81, 0x00,                      /*   INPUT (Data,Ary,Abs) */

// 	/* 5 LEDs for num lock etc, 3 left for advanced, custom usage */
// 	0x05, 0x08,						 /*   USAGE_PAGE (LEDs) */
// 	0x19, 0x01,						 /*   USAGE_MINIMUM (Num Lock) */
// 	0x29, 0x08,						 /*   USAGE_MAXIMUM (Kana + 3 custom)*/
// 	0x95, 0x08,						 /*   REPORT_COUNT (8) */
// 	0x75, 0x01,						 /*   REPORT_SIZE (1) */
// 	0x91, 0x02,						 /*   OUTPUT (Data,Var,Abs) */

//     /* 6 Keyboard keys */
//     0x05, 0x07,                      /*   USAGE_PAGE (Keyboard) */
//     0x95, 0x06,                      /*   REPORT_COUNT (6) */
//     0x75, 0x08,                      /*   REPORT_SIZE (8) */
//     0x15, 0x00,                      /*   LOGICAL_MINIMUM (0) */
//     0x26, 0xE7, 0x00,                /*   LOGICAL_MAXIMUM (231) */
//     0x19, 0x00,                      /*   USAGE_MINIMUM (Reserved (no event indicated)) */
//     0x29, 0xE7,                      /*   USAGE_MAXIMUM (Keyboard Right GUI) */
//     0x81, 0x00,                      /*   INPUT (Data,Ary,Abs) */

//     /* End */
//     0xc0                            /* END_COLLECTION */
// };

static const uint8_t _hidReportDescriptor[] PROGMEM = {
    // HID_USAGE_PAGE_SENSOR,
    // HID_USAGE_SENSOR_TYPE_COLLECTION,
    // HID_COLLECTION(Application),

    //     HID_REPORT_ID(1),
        HID_USAGE_PAGE_SENSOR, // USAGE_PAGE (Sensor)

        HID_USAGE_SENSOR_TYPE_LIGHT_AMBIENTLIGHT, // USAGE (AmbientLight)
        HID_COLLECTION(Physical),
        // HID_COLLECTION(Application),
            
//ass
// feature reports (xmit/receive)
            HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_PROPERTY_SENSOR_CONNECTION_TYPE, // NAry
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(2),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_INTEGRATED_SEL,
                HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_ATTACHED_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(5),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_SEL_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL_WAKE_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_ALL_EVENTS_SEL_WAKE_SEL,
                HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_THRESHOLD_EVENTS_WAKE_SEL_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_PROPERTY_POWER_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(2),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE_UNDEFINED_SEL,
                HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D1_LOW_POWER_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D2_STANDBY_WITH_WAKE_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D3_SLEEP_WITH_WAKE_SEL,
                // HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D4_POWER_OFF_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(6),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),
            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_STATE_UNKNOWN_SEL_SEL,
                HID_USAGE_SENSOR_STATE_READY_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NO_DATA_SEL_SEL,
                HID_USAGE_SENSOR_STATE_INITIALIZING_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ERROR_SEL_SEL,
                HID_FEATURE(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_PROPERTY_REPORT_INTERVAL,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            HID_REPORT_SIZE(32),
            HID_REPORT_COUNT(1),
            HID_UNIT_EXPONENT(0),
            HID_FEATURE(Data_Var_Abs),
//ass

            HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE, HID_USAGE_SENSOR_DATA_MOD_CHANGE_SENSITIVITY_REL_PCT),
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_16(0x10, 0x27), // 10000 = 0.00 to 100.00 percent with 2 digits past decimal point
            HID_REPORT_SIZE(16),
            HID_REPORT_COUNT(1),
            HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digits past decimal point
            HID_FEATURE(Data_Var_Abs),

            HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE, HID_USAGE_SENSOR_DATA_MOD_MAX),
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            HID_REPORT_SIZE(32),
            HID_REPORT_COUNT(1),
            HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digit past decimal point
            HID_FEATURE(Data_Var_Abs),

            HID_USAGE_SENSOR_DATA(HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE, HID_USAGE_SENSOR_DATA_MOD_MIN),
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            HID_REPORT_SIZE(32),
            HID_REPORT_COUNT(1),
            HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digit past decimal point
            HID_FEATURE(Data_Var_Abs),

            // add this definition if required by the specific application
            // HID_USAGE_SENSOR_PROPERTY_RESPONSE_CURVE,
            // HID_LOGICAL_MIN_16(0x01, 0x80), //    LOGICAL_MINIMUM (-32767)
            // HID_LOGICAL_MAX_16(0xFF, 0x7F), //    LOGICAL_MAXIMUM (32767)
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(10),   //as required for n pair of values
            // HID_UNIT_EXPONENT(0x0), // scale default unit to provide 0 digits past the decimal point
            // HID_FEATURE(Data_Var_Abs),

            //input reports (transmit)
            // HID_USAGE_PAGE_SENSOR,
            HID_USAGE_SENSOR_STATE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(6),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),

            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_STATE_UNKNOWN_SEL_SEL,
                HID_USAGE_SENSOR_STATE_READY_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NOT_AVAILABLE_SEL_SEL,
                HID_USAGE_SENSOR_STATE_NO_DATA_SEL_SEL,
                HID_USAGE_SENSOR_STATE_INITIALIZING_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ACCESS_DENIED_SEL_SEL,
                HID_USAGE_SENSOR_STATE_ERROR_SEL_SEL,
                HID_INPUT(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_EVENT,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_8(5),
            HID_REPORT_SIZE(8),
            HID_REPORT_COUNT(1),

            HID_COLLECTION(Logical),
                HID_USAGE_SENSOR_EVENT_UNKNOWN_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_PROPERTY_CHANGED_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_POLL_RESPONSE_SEL_SEL,
                HID_USAGE_SENSOR_EVENT_CHANGE_SENSITIVITY_SEL_SEL,
                HID_INPUT(Data_Arr_Abs),
            HID_END_COLLECTION,

            HID_USAGE_SENSOR_DATA_LIGHT_ILLUMINANCE,
            HID_LOGICAL_MIN_8(0),
            HID_LOGICAL_MAX_32(0xFF, 0xFF, 0xFF, 0xFF),
            HID_UNIT_EXPONENT(0x0E), // scale default unit to provide 2 digits past decimal point
            HID_REPORT_SIZE(32),
            HID_REPORT_COUNT(1),
            HID_INPUT(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA_LIGHT_COLOR_TEMPERATURE,
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_16(0xFF, 0xFF),
            // HID_UNIT_EXPONENT(0),
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(1),
            // HID_INPUT(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA_LIGHT_CHROMATICITY_X,
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_16(0xFF, 0xFF),
            // HID_UNIT_EXPONENT(0x0C), // scale default unit to provide 4 digits past decimal point
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(1),
            // HID_INPUT(Data_Var_Abs),

            // HID_USAGE_SENSOR_DATA_LIGHT_CHROMATICITY_Y,
            // HID_LOGICAL_MIN_8(0),
            // HID_LOGICAL_MAX_16(0xFF, 0xFF),
            // HID_UNIT_EXPONENT(0x0C), // scale default unit to provide 4 digits past decimal point
            // HID_REPORT_SIZE(16),
            // HID_REPORT_COUNT(1),
            // HID_INPUT(Data_Var_Abs),

        HID_END_COLLECTION,
    // HID_END_COLLECTION
};

BetterSensor_::BetterSensor_(void) : PluggableUSBModule(1, 1, epType), protocol(HID_REPORT_PROTOCOL), idle(1), featureReport(NULL), featureLength(0)
{
	epType[0] = EP_TYPE_INTERRUPT_IN;
    _inputReport.ucSensorState = HID_USAGE_SENSOR_STATE_READY_SEL_ENUM;
    _inputReport.ucEventType = HID_USAGE_SENSOR_EVENT_STATE_CHANGED_SEL_ENUM;
    _inputReport.usIlluminanceValue = 0;
    featureState = 
    {
        HID_USAGE_SENSOR_PROPERTY_CONNECTION_TYPE_PC_EXTERNAL_SEL_ENUM,
        HID_USAGE_SENSOR_PROPERTY_REPORTING_STATE_NO_EVENTS_SEL_ENUM,
        HID_USAGE_SENSOR_PROPERTY_POWER_STATE_D0_FULL_POWER_ENUM,
        HID_USAGE_SENSOR_STATE_READY_SEL_ENUM,
        500UL,
        1000,
        20000000UL,
        0UL,
    };
	PluggableUSB().plug(this);
}

int BetterSensor_::getInterface(uint8_t* interfaceCount)
{
	*interfaceCount += 1; // uses 1
	HIDDescriptor hidInterface = {
		D_INTERFACE(pluggedInterface, 1, USB_DEVICE_CLASS_HUMAN_INTERFACE, HID_SUBCLASS_NONE, HID_PROTOCOL_NONE),
		D_HIDREPORT(sizeof(_hidReportDescriptor)),
		D_ENDPOINT(USB_ENDPOINT_IN(pluggedEndpoint), USB_ENDPOINT_TYPE_INTERRUPT, USB_EP_SIZE, 0x01)
	};
	return USB_SendControl(0, &hidInterface, sizeof(hidInterface));
}

int BetterSensor_::getDescriptor(USBSetup& setup)
{
	// Check if this is a HID Class Descriptor request
	if (setup.bmRequestType != REQUEST_DEVICETOHOST_STANDARD_INTERFACE) { return 0; }
	if (setup.wValueH != HID_REPORT_DESCRIPTOR_TYPE) { return 0; }

	// In a HID Class Descriptor wIndex cointains the interface number
	if (setup.wIndex != pluggedInterface) { return 0; }

	// Reset the protocol on reenumeration. Normally the host should not assume the state of the protocol
	// due to the USB specs, but Windows and Linux just assumes its in report mode.
	protocol = HID_REPORT_PROTOCOL;

	return USB_SendControl(TRANSFER_PGM, _hidReportDescriptor, sizeof(_hidReportDescriptor));
}

bool BetterSensor_::setup(USBSetup& setup)
{
	if (pluggedInterface != setup.wIndex) {
		return false;
	}

	uint8_t request = setup.bRequest;
	uint8_t requestType = setup.bmRequestType;

	if (requestType == REQUEST_DEVICETOHOST_CLASS_INTERFACE)
	{
        // Serial.println("REQUEST_DEVICETOHOST_CLASS_INTERFACE");
		if (request == HID_GET_REPORT) {
			// TODO: HID_GetReport();
                        // TODO: Send latest data
            // Serial.println("HID_GET_REPORT");            
            if (setup.wValueH == HID_REPORT_TYPE_FEATURE) {
                // send feature report
                // Serial.println("HID_REPORT_TYPE_FEATURE");
                // USB_Send(pluggedEndpoint, &id, 1);
                // auto rep = USB_SendControl(TRANSFER_PGM, &featureState, sizeof(featureState));
                // USB_Send(pluggedEndpoint | TRANSFER_PGM, &featureState, sizeof(featureState));
            	// auto rep = USB_Send(pluggedEndpoint | TRANSFER_RELEASE, &_inputReport, sizeof(_inputReport));
                return true;
            }
            if (setup.wValueH == HID_REPORT_TYPE_INPUT) {
                // send sensor state
                // Serial.println("HID_REPORT_TYPE_INPUT");
            	USB_Send(pluggedEndpoint | TRANSFER_RELEASE, &_inputReport, sizeof(_inputReport));
                return true;
            }

			return true;
		}
		if (request == HID_GET_PROTOCOL) {
			// TODO improve
#ifdef __AVR__
			UEDATX = protocol;
#endif
			return true;
		}
		if (request == HID_GET_IDLE) {
			// TODO improve
#ifdef __AVR__
			UEDATX = idle;
#endif
			return true;
		}
	}

	if (requestType == REQUEST_HOSTTODEVICE_CLASS_INTERFACE)
	{
        // Serial.println("REQUEST_HOSTTODEVICE_CLASS_INTERFACE");
		if (request == HID_SET_PROTOCOL) {
			protocol = setup.wValueL;
			return true;
		}
		if (request == HID_SET_IDLE) {
            // Serial.println("HID_SET_IDLE");
			idle = setup.wValueL;
			return true;
		}
		if (request == HID_SET_REPORT)
		{
            // Serial.println("HID_SET_REPORT");
			// Check if data has the correct length afterwards
			int length = setup.wLength;

			// Feature (set feature report)
			if(setup.wValueH == HID_REPORT_TYPE_FEATURE){
				// No need to check for negative featureLength values,
				// except the host tries to send more then 32k bytes.
				// We dont have that much ram anyways.
                // Serial.println("HID_REPORT_TYPE_FEATURE");
                // Serial.println(length);
				if (length == sizeof(featureState)) {
                    // Serial.println("WRITING");
					USB_RecvControl(&featureState, sizeof(featureState));

					// Block until data is read (make length negative)
					// disableFeatureReport();
					return true;
				}
				// TODO fake clear data?
			}

			// Output (set led states)
			else if(setup.wValueH == HID_REPORT_TYPE_OUTPUT){
				// if(length == sizeof(leds)){
				// 	USB_RecvControl(&leds, length);
                    // Serial.println("HID_REPORT_TYPE_OUTPUT");
					return true;
				// }
			}

			// Input (set HID report)
			else if(setup.wValueH == HID_REPORT_TYPE_INPUT)
			{
                // Serial.println("HID_REPORT_TYPE_INPUT");
				if(length == sizeof(_inputReport)){
					USB_RecvControl(&_inputReport, length);
					return true;
				}
			}
		}
	}

	return false;
}

// uint8_t BetterSensor_::getLeds(void){
//     return leds;
// }

// uint8_t BetterSensor_::getProtocol(void){
//     return protocol;
// }

void BetterSensor_::updateReport(HID_ULONG val) {
    _inputReport.ucSensorState = HID_USAGE_SENSOR_STATE_READY_SEL_ENUM;
    _inputReport.ucEventType = HID_USAGE_SENSOR_EVENT_DATA_UPDATED_SEL_ENUM;
    _inputReport.usIlluminanceValue = val;
}

void BetterSensor_::setFeatureState(ALS_FEATURE_REPORT rep) {
    featureState = rep;
}

int BetterSensor_::send(void){
	return USB_Send(pluggedEndpoint | TRANSFER_RELEASE, &_inputReport, sizeof(_inputReport));
}

// void BetterSensor_::wakeupHost(void){
// #ifdef __AVR__
// 	USBDevice.wakeupHost();
// #endif
// }


BetterSensor_ BetterSensor;


