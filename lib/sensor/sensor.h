#ifndef SENSOR_h
#define SENSOR_h

#include "HID.h"
#include "hello.h"

#if !defined(_USING_HID)

#warning "Using legacy HID core (non pluggable)"

#else

//  Low level key report: up to 6 keys and shift, ctrl etc at once
typedef struct _ALS_FEATURE_REPORT
{
    //common properties
    // HID_UCHAR ucReportId;
    HID_UCHAR ucConnectionType;
    HID_UCHAR ucReportingState;
    HID_UCHAR ucPowerState;
    HID_UCHAR ucSensorState;
    HID_ULONG ulReportInterval;

    //properties specific to this sensor
    // HID_USHORT usIlluminanceChangeSensitivity;
    HID_ULONG usIlluminanceMaximum;
    HID_ULONG usIlluminanceMinimum;

    //add this definition if required by the specific application
    // HID_USHORT usResponseCurve[5][2]; //10 elements matches descriptor

} ALS_FEATURE_REPORT, *PALS_FEATURE_REPORT;

typedef struct
{
      //common properties
    // HID_UCHAR ucReportId;
    // HID_UCHAR ucConnectionType;
    // HID_UCHAR ucReportingState;
    // HID_UCHAR ucPowerState;
    // HID_UCHAR ucSensorState;
    // HID_ULONG ulReportInterval;

    // //properties specific to this sensor
    // // HID_USHORT usIlluminanceChangeSensitivity;
    // HID_ULONG usIlluminanceMaximum;
    // HID_ULONG usIlluminanceMinimum;
    //common values
    // HID_UCHAR ucReportId;
    HID_UCHAR ucSensorState;
    HID_UCHAR ucEventType;

    //values specific to this sensor
    HID_ULONG usIlluminanceValue;
    // HID_USHORT usColorTempValue;
    // HID_USHORT usChromaticityXValue;
    // HID_USHORT usChromaticityYValue;

} ALS_INPUT_REPORT;

class Sensor_ // : public Print
{
private:
  ALS_FEATURE_REPORT _featureReport;
  ALS_INPUT_REPORT _inputReport;

  int sendReport(ALS_INPUT_REPORT* inpRep);
public:
  Sensor_(void);
  void begin(void);
  void end(void);
  size_t report(HID_ULONG lux);
};
extern Sensor_ Sensor;

#endif
#endif

